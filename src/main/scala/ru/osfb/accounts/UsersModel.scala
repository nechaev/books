package ru.osfb.accounts

import java.util.UUID

import ru.osfb.db.DatabaseProfileComponent

/**
 * Created by sgl on 12.12.14.
 */
trait UsersModel { this: DatabaseProfileComponent =>
  import databaseProfile.simple._

  class UsersTable(t: Tag) extends Table[User](t, "USERS") {
    def id = column[UUID]("ID", O.PrimaryKey, O.Length(40, varying = true))
    def name = column[String]("NAME", O.Length(200, varying = true))
    def email = column[String]("EMAIL", O.Length(100, varying = true))
    def * = (id, name, email) <> (User.tupled, User.unapply)
  }

  val users = TableQuery[UsersTable]

}

case class User(id: UUID, name: String, email: String)
