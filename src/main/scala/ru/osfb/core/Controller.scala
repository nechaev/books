package ru.osfb.core

import java.time.{ZoneId, Instant}
import java.time.format.DateTimeFormatter

import play.api.libs.json._
import spray.routing.Route

/**
 * Created by sgl on 13.12.14.
 */
trait Controller {
  def route: Route

  val instantFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

  implicit val instantFormat = Format[Instant](
    Reads({
      case JsString(str) => JsSuccess(Instant.from(instantFormatter.parse(str)))
      case _ => JsError("Json string expected")
    }),
    Writes(i => JsString(instantFormatter.format(i.atZone(ZoneId.systemDefault()))))
  )
}
