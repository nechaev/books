package ru.osfb.core

import akka.actor.ActorSystem

/**
 * Created by sgl on 13.12.14.
 */
trait ActorSystemComponent {
  implicit def actorSystem: ActorSystem
}

class ActorSystemImpl(name: String) { this: ConfigurationComponent =>
  private lazy val actorSystemImpl = ActorSystem(name, configuration)
  trait Component extends ActorSystemComponent {
    override implicit def actorSystem: ActorSystem = actorSystemImpl
  }
}
