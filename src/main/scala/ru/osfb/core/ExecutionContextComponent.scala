package ru.osfb.core

import scala.concurrent.ExecutionContext

/**
 * Created by sgl on 27.12.14.
 */
trait ExecutionContextComponent {
  implicit def executionContext: ExecutionContext
}
