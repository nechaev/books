package ru.osfb.core

import java.util.concurrent
import java.util.concurrent.TimeUnit

import org.infinispan.commons.util.concurrent.{FutureListener, NotifyingFuture}
import org.infinispan.manager.DefaultCacheManager

import scala.concurrent.{Promise, Future}
import scala.concurrent.duration.Duration
import scala.language.implicitConversions
import scala.reflect.ClassTag
import scala.util.Try

class InfinispanCacheImpl extends Cache { this: ConfigurationComponent with ExecutionContextComponent =>

  lazy val cache = new DefaultCacheManager(configValue[String]("cache.infinispan-config")).getCache[String, Any]

  private implicit def notifyingFutureToScalaFuture[T](nf: NotifyingFuture[T]): Future[T] = {
    val promise = Promise[T]()
    nf.attachListener(new FutureListener[T] {
      override def futureDone(future: concurrent.Future[T]): Unit = promise.complete(Try(future.get()))
    })
    promise.future
  }

  override def +=(entry: (String, Any))(implicit ttl: Duration): Future[Unit] = {
    cache.putAsync(entry._1, entry._2, ttl.toMillis, TimeUnit.MILLISECONDS).map(_ => ())
  }

  override def -=(key: String): Future[Unit] = cache.removeAsync(key).map(_ => ())

  override def apply[T](key: String)(implicit t: ClassTag[T]): Future[T] = cache.getAsync(key).mapTo[T]
}