package ru.osfb.core

import java.util.concurrent.TimeUnit

import com.typesafe.config.{ConfigFactory, Config}

import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.reflect.runtime.universe._

/**
 * Created by sgl on 13.12.14.
 */
trait ConfigurationComponent {
  protected def configuration: Config

  private val StringTag = typeTag[String]
  private val ScalaDurationTag = typeTag[scala.concurrent.duration.Duration]
  private val JavaDurationTag = typeTag[java.time.Duration]
  private val FiniteDurationTag = typeTag[FiniteDuration]
  private val StringListTag = typeTag[List[String]]
  private val BooleanTag = typeTag[Boolean]

  private def getOpt[T](path: String)(implicit tag: TypeTag[T]):Option[T] = if (configuration.hasPath(path)) (tag match {
    case StringTag => Some(configuration.getString(path))
    case TypeTag.Int => Some(configuration.getInt(path))
    case TypeTag.Float => Some(configuration.getDouble(path).toFloat)
    case ScalaDurationTag => Some(configuration.getDuration(path, TimeUnit.MILLISECONDS).millis)
    case JavaDurationTag => Some(java.time.Duration.ofMillis(configuration.getDuration(path, TimeUnit.MILLISECONDS)))
    case FiniteDurationTag => Some(configuration.getDuration(path, TimeUnit.MILLISECONDS).millis)
    case StringListTag => Some(configuration.getStringList(path).toList)
    case BooleanTag => Some(configuration.getBoolean(path))
    case _ => throw new IllegalArgumentException(s"Configuration option type $tag not implemented")
  }).asInstanceOf[Option[T]] else None
  def configValue[T](path: String, default: =>T)(implicit tag: TypeTag[T]) = getOpt(path).getOrElse(default)
  def configValue[T](path: String)(implicit tag: TypeTag[T]) = getOpt(path).getOrElse(throw new RuntimeException(s"Configuration value at $path not found"))

}

class ConfigurationImpl {
  val configImpl = ConfigFactory.load()
  trait Component extends ConfigurationComponent {
    override protected def configuration: Config = configImpl
  }
}