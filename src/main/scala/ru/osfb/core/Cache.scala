package ru.osfb.core

import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.reflect.ClassTag

/**
 * Created by sgl on 22.12.14.
 */
trait Cache {
  def +=(entry: (String, Any))(implicit ttl: Duration): Future[Unit]
  def apply[T](key: String)(implicit t: ClassTag[T]): Future[T]
  def -=(key: String): Future[Unit]
}

trait CacheComponent {
  def cache: Cache
}