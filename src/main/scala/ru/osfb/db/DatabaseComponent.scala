package ru.osfb.db

/**
 * Created by sgl on 13.12.14.
 */
trait DatabaseComponent[T <: DatabaseProfileComponent] {
  val model: T
  import model.databaseProfile.simple._
  def database: Database
  def withDbSession[RT](block: Session => RT) = database.withSession(block)
  def withDbTransaction[RT](block: Session => RT) = database.withTransaction(block)
}
