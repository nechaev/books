package ru.osfb.db

import scala.slick.driver.JdbcProfile

/**
 * Created by sgl on 12.12.14.
 */
trait DatabaseProfileComponent {
  val databaseProfile: JdbcProfile
}
