package ru.osfb.books

import ru.osfb.core.Session
import spray.http.StatusCodes
import spray.routing.{Directive0, SimpleRoutingApp}
import ComponentWiring._

/**
 * Created by sgl on 13.12.14.
 */
object BooksApplicaton extends App with SimpleRoutingApp with configuration.Component
  with ActorSystemComponentImpl with CacheComponentImpl with DefaultExecutionContextImpl {
  lazy val authEndpoint: String = configValue("oauth.authEndpoint")
  lazy val tokenEndpoint: String = configValue("oauth.tokenEndpoint")
  lazy val clientId: String = configValue("oauth.clientId")
  lazy val clientSecret: String = configValue("oauth.clientSecret")

  private val SESSION_COOKIE = "SESSION_ID"

  lazy val controllers = Seq(new BooksController)

  startServer(interface = configValue[String]("http.interface"), port = configValue[Int]("http.port")) {
    /*
    (pathEndOrSingleSlash & ) { session
      authenticate()
    } ~ */ 
    pathPrefix("api") {
      controllers.map(_.route).reduce(_ ~ _)
    } ~ pathPrefix("lib") {
      getFromDirectory(configValue[String]("http.libResourcePath"))
    } ~ pathEndOrSingleSlash {
      redirect("index.html", StatusCodes.Found)
    } ~ getFromDirectory(configValue[String]("http.buildResourcePath")) ~
    getFromDirectory(configValue[String]("http.staticResourcePath"))
  }

  /*

  def authenticate = redirect(s"$authEndpoint?response_type=code&client_id", StatusCodes.Found)
  def cacheSessionKey(sessionId: String) = "session/" + sessionId

  val session: Directive0 = optionalCookie(SESSION_COOKIE
  ) flatMap {
    case Some(sessionCookie) => onComplete(cache[Session](cacheSessionKey(sessionCookie.content)))
    case None => authenticate
  }
  */
}
