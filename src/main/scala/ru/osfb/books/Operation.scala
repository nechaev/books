package ru.osfb.books

import java.time.Instant

/**
 * Created by sgl on 10.01.15.
 */
case class Operation(id: Option[Int], timestamp: Instant, category: String, description: Option[String], sum: BigDecimal)
