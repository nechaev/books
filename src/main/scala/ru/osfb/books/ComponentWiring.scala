package ru.osfb.books

import akka.actor.ActorSystem
import ru.osfb.core._
import ru.osfb.db.{DatabaseComponent, DatabaseProfileComponent}

import scala.concurrent.ExecutionContext
import scala.slick.driver.{PostgresDriver, JdbcProfile}

/**
 * Created by sgl on 14.12.14.
 */
object ComponentWiring {
  lazy val configuration = new ConfigurationImpl
  lazy val actorSystemImpl = ActorSystem()
  trait ActorSystemComponentImpl extends ActorSystemComponent {
    override implicit def actorSystem: ActorSystem = actorSystemImpl
  }
  trait DefaultExecutionContextImpl extends ExecutionContextComponent {
    override implicit def executionContext: ExecutionContext = actorSystemImpl.dispatcher
  }
  private lazy val cacheImpl = new InfinispanCacheImpl with configuration.Component
    with DefaultExecutionContextImpl
  trait CacheComponentImpl extends CacheComponent {
    override def cache: Cache = cacheImpl
  }

  private val modelImpl = new BooksModel with DatabaseProfileComponent {
    override val databaseProfile: JdbcProfile = PostgresDriver
  }

  private lazy val databaseImpl = {
    import modelImpl.databaseProfile.simple._
    Database.forURL(configuration.configImpl.getString("database.url"), databa)
  }

  trait DatabaseComponentImpl extends DatabaseComponent[BooksModel] {
    override val model: BooksModel = modelImpl
    override def database: DatabaseComponentImpl#model#databaseProfile#simple
  }
}
