package ru.osfb.books

import java.time.Instant
import java.sql.Timestamp

import ru.osfb.db.DatabaseProfileComponent

/**
 * Created by sgl on 11.01.15.
 */
trait BooksModel { this: DatabaseProfileComponent =>
  import databaseProfile.simple._

  implicit val instantColumnType = MappedColumnType.base[Instant, Timestamp](
    { i => new Timestamp(i.toEpochMilli)},
    { d => Instant.ofEpochMilli(d.getTime)}
  )

  class OperationsTable(t: Tag) extends Table[Operation](t, "OPERATIONS") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def timestamp = column[Instant]("TIMESTAMP")
    def category = column[String]("CATEGORY", O.Length(50, varying = true))
    def description = column[Option[String]]("DESCRIPTION", O.Length(100, varying = true))
    def sum = column[BigDecimal]("SUM")
    def * = (id.?, timestamp, category, description, sum) <> (Operation.tupled, Operation.unapply)
    def timestampIdx = index("OPERATIONS_TIMESTAMP_IDX", timestamp, unique = false)
  }

  val operations = TableQuery[OperationsTable]

}
