package ru.osfb.books

import ru.osfb.db.DatabaseProfileComponent

/**
 * Created by sgl on 13.12.14.
 */
trait CommonModel { this: DatabaseProfileComponent =>
  import databaseProfile.simple._

  class PartiesTable(t: Tag) extends Table[Party](t, "PARTIES") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def name = column[String]("NAME", O.Length(100, varying = true))

    def * = (id.?, name) <> (Party.tupled, Party.unapply)
    def nameIdx = index("PARTY_NAME_IDX", name)
  }

  val parties = TableQuery[PartiesTable]

  class BanksTable(t: Tag) extends Table[(Int, String)](t, "BANKS") {
    def partyId = column[Int]("ID", O.PrimaryKey)
    def bic = column[String]("BIC", O.Length(11, varying = true))
    def * = (partyId, bic)
    def party = foreignKey("BANKS_PTY_FK", partyId, parties)(_.id)
  }

  val banks = TableQuery[BanksTable]

}

case class Party(idOpt: Option[Int], name: String)

case class Bank(id: Int, name: String, bic: String)

case class Country(code: String, name: String)