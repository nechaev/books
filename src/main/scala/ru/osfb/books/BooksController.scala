package ru.osfb.books

import java.time.{Instant, LocalDate}

import play.api.libs.json.{JsString, Json}
import ru.osfb.core.{ConfigurationComponent, ExecutionContextComponent, Controller}
import ru.osfb.db.DatabaseComponent
import spray.httpx.PlayJsonSupport._
import spray.routing.{Directives, Route}
import scala.slick.jdbc.StaticQuery.interpolation

/**
 * Created by sgl on 09.01.15.
 */
class BooksController extends Controller with Directives {
  this: ExecutionContextComponent with DatabaseComponent[BooksModel] with ConfigurationComponent =>

  implicit val operationWrites = Json.writes[Operation]
  lazy val listLimit: Int = configValue("listLimit")

  override def route: Route = path("get-categories") {
    //complete(List("Транспорт", "Питание", "Жилье", "Развлечения"))
    val lastCategories = withDbSession { implicit sess =>
      sql"SELECT DISTINCT CATEGORY FROM OPERATIONS ORDER BY TIMESTAMP DESC LIMIT 5".as[String].list
    }
    complete(lastCategories)
  } ~ path("get-operations") {
    model.operations.sortBy(_.timestamp.desc).take(listLimit)
    val ops = Seq(Operation(Some(1), Instant.now(), "Прочее", Some("Фигня всякая"), 123.45))
    complete(ops)
  }
}
