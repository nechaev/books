if (typeof window.CustomEvent !== 'function') {
    window.CustomEvent = function(type, eventInitDict) {
        var newEvent = document.createEvent('CustomEvent');
        newEvent.initCustomEvent(type,
            !!(eventInitDict && eventInitDict.bubbles),
            !!(eventInitDict && eventInitDict.cancelable),
            (eventInitDict ? eventInitDict.details : null));
        return newEvent;
    };
}
