import { Future, List } from 'functional-js/enumerators';
import { forEach } from 'functional-js/iteratees';
import { getOperations } from 'api';

var operations = Future(getOperations).flatMap(List);

export default class LatestExpenses extends HTMLElement {
    createdCallback() {
        var content = document.importNode(LatestExpenses.templates.root, true);
        this.appendChild(content);
        this.updateList();
    }
    updateList() {
        var table = this.querySelector('tbody');
        $(table).empty();
        operations(forEach(row => {
            var tableRow = document.importNode(LatestExpenses.templates.row, true);
            Array.prototype.forEach.call(tableRow.querySelectorAll('*[name]'), elem => {
                elem.innerHTML = row[elem.getAttribute('name')];
            });
            table.appendChild(tableRow);
        }));
    }

}