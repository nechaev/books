import { forEachCancellable } from 'functional-js/iteratees';

export default class XText extends HTMLElement {
    attachedCallback() {
        this.input = forEachCancellable(text => this.innerHTML = text);
        $(this).trigger('x-element-ready');
    }
    detachedCallback() {
        this.input.cancel();
    }
}
