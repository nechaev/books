import { getCategories } from 'api';
import { Future, List, mapDone } from 'functional-js/enumerators';
import { forEach } from 'functional-js/iteratees';

var categoriesStore = Future(getCategories).flatMap(List);

export default class ExpenseCategories extends HTMLElement {
    createdCallback() {
        var content = document.importNode(ExpenseCategories.templates.root, true);
        var container = content.querySelector('#categoryContainer');
        var categoryBuilder = forEach(category => {
            var item = document.importNode(ExpenseCategories.templates.item, true);
            item.querySelector('div.category-text').innerHTML = category;
            container.appendChild(item);
        });
        mapDone(categoriesStore(categoryBuilder)).forEach(() => {
            var firstItem = container.querySelector('div.category-item');
            $(firstItem).addClass('category-item-selected');
            this.value = firstItem.querySelector('div.category-text').innerHTML;
        });
        this.appendChild(content);
        var $this = $(this);
        var $input = $this.find('input[name="category"]');
        $this.on('click', 'div.category-item', event => {
            $this.find('div.category-item').removeClass('category-item-selected');
            var $selected = $(event.target);
            if (!$selected.hasClass('category-item')) $selected = $selected.parents('div.category-item');
            $selected.addClass('category-item-selected');
            if (!$selected.hasClass('custom-category')) {
                this.value = $selected.find('div.category-text').html();
                $input.val('');
            }

        });
        $input.on('input', event => {
            $this.find('div.category-item').removeClass('category-item-selected');
            $this.find('div.custom-category').addClass('category-item-selected');
            this.value = $input.val();
        });
        $input.on('focus', event => {
            event.target.select();
        });
    }
}
