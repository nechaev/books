export default class FloatingLabelDecorator extends HTMLElement {
    createdCallback() {
        var content = document.importNode(FloatingLabelDecorator.templates.root, true);
        var container = content.querySelector('div.form-group');
        var label = content.querySelector('label');
        var input = this.querySelector('input');
        container.appendChild(input);
        this.appendChild(content);
        function updateLabel() {
            if (input.value == '') label.innerHTML='&nbsp;';
            else label.innerHTML = input.getAttribute('placeholder');
        }
        $(input).on('input', updateLabel);
        updateLabel();
    }
}