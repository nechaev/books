import { Future } from 'functional-js/monads';

const API_BASE_URL = '/api/';

function apiMethod(name, options) {
    return params => new Future($.ajax(API_BASE_URL + name, {
        type: 'POST',
        data: params !== undefined ? JSON.stringify(params) : undefined,
        dataType: 'json'
    }));
}

export var getCategories = apiMethod('get-categories');
export var addExpense = apiMethod('add-expense');
export var getOperations = apiMethod('get-operations');
