import { One, Channel } from 'functional-js/enumerators';
import { addExpense } from 'api';

export default class ExpenseEntry extends HTMLElement {
    createdCallback() {
        var content = document.importNode(ExpenseEntry.templates.root, true);
        this.appendChild(content);
        $(this).find('#addEntry').click(() => {
            addExpense()
        });
    }
}