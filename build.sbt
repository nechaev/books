lazy val books =  project.in(file(".")).settings(
    organization := "ru.osfb",
    scalaVersion := "2.11.6",
    scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation"),
    resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
    libraryDependencies ++= Seq(
        "org.scala-lang" % "scala-reflect" % "2.11.6",
        "com.typesafe.akka" %% "akka-actor" % "2.3.9",
        "io.spray" %% "spray-can" % "1.3.2",
        "io.spray" %% "spray-routing" % "1.3.2",
        "com.typesafe.slick" %% "slick" % "2.1.0",
        "com.typesafe.play" %% "play-json" % "2.3.8",
        "org.postgresql" % "postgresql" % "9.3-1102-jdbc41",
        "org.infinispan" % "infinispan-embedded" % "7.1.0.Final"
    )
)